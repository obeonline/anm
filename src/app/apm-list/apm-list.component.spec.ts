import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApmListComponent } from './apm-list.component';

describe('ApmListComponent', () => {
  let component: ApmListComponent;
  let fixture: ComponentFixture<ApmListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApmListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApmListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
