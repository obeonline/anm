import { Component, OnInit } from '@angular/core';
import * as appData from '../../test/app-list-1.json';

@Component({
  selector: 'apm-list',
  templateUrl: './apm-list.component.html',
  styleUrls: ['./apm-list.component.css']
})
export class ApmListComponent implements OnInit {

  constructor() {}

  apps = [
    {
        "apmId": "AD00001234",
        "appName" : "myQ",
        "response" : "true"
    },
    {
        "apmId": "AD00005678",
        "appName" : "Document Intake",
        "response" : "true"
    }
  ];

  ngOnInit(): void {
    console.log(this.apps)
  }

}
